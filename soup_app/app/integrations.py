import json
import http.client
from .models import *

class Integration:

    def _get_webhooks():
        return [webhook.url.replace("https://", "") for webhook in Webhook.objects.all()]
    
    def _get_projects_info():
        projects = list(Project.objects.all().values())
        data = {"projects": [{"id":project["id"], "name" : project["name"], "short_name" : project['short_name'], 'is_archived': project['is_archived'] } 
                for project in projects]}
        return json.dumps(data)

    def send_projects_info():
        hooks = Integration._get_webhooks()
        projects_info = Integration._get_projects_info()

        try:           
            for hook in hooks:
                conn = http.client.HTTPSConnection(hook)
                headers = { 'Content-Type': 'application/json' }
                conn.request("POST", '/', body=projects_info, headers=headers)
        except Exception as e:
            print(f"Integration error: {e}")
    
