from .models import Task, TaskStatus, TaskType, Project, ProjectParticipant
from django.db import models


def get_object_or_none(identifier: object, model: models.Model):
    try:
        result = model.objects.get(id=identifier)
    except model.DoesNotExist:
        result = None
    return result
