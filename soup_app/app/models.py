import datetime

from django.db import models
from django.db.models import Q
from django.contrib.auth.models import AbstractUser
import uuid


class User(AbstractUser):
    # override email from Django's default user model
    email = models.EmailField(null=True, blank=True)
    second_name = models.CharField(max_length=150, blank=True)

    def background_color(self):
        return self.id % 5

    def is_admin(self):
        return self.is_staff

    def __str__(self):
        if self.first_name and self.last_name:
            return self.first_name + ' ' + self.last_name
        else:
            return self.username

    def get_projects(self):
        if self.is_admin:
            return Project.objects.all()
        else:
            return self.projects.all()

    def get_participants(self):
        return self.participants.all()

    def get_fio(self):
        return ' '.join([self.last_name or '', self.first_name or '', self.second_name or '']).strip()

    def get_tasks(self):
        return Task.objects.filter(
            Q(task_executor__in=self.participants.all()) | Q(task_creator__in=self.participants.all())
        ).order_by('id')

    def get_full_name(self):
        return self.get_fio() or self.username

    class Meta:
        db_table = 'user'


class ProjectRole(models.Model):
    name = models.TextField(unique=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'project_role'


class Project(models.Model):
    project_identifier = models.UUIDField(default=uuid.uuid4)
    name = models.TextField()
    short_name = models.CharField(max_length=255)
    is_archived = models.BooleanField(default=False)

    users = models.ManyToManyField(User, related_name='projects', through='ProjectParticipant')

    def is_accessible_by(self, user: User):
        return user.is_admin() or user in self.users.all()

    def is_manageable_by(self, user: User):
        return user.is_admin() or user in self.get_head_users()

    def get_head_users(self):
        return [participant.user for participant in self.participants.filter(roles__name='Руководитель проекта').all()]

    def get_participants(self):
        return self.participants.all()

    def get_non_participant_users(self):
        return User.objects.exclude(id__in=self.participants.values_list('user_id', flat=True))

    def get_tasks(self):
        return self.tasks.all()

    def archive(self):
        self.is_archived = True
        self.save()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'project'


class ProjectParticipant(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='participants', db_column='user_id')
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='participants', db_column='project_id')

    roles = models.ManyToManyField(ProjectRole, through='ProjectParticipantRole')

    def get_tasks(self):
        created_tasks = self.created_tasks.all()
        executed_tasks = self.executed_tasks.all()
        return created_tasks.union(executed_tasks)

    def get_roles(self):
        return self.roles.all()

    def __str__(self):
        return str(self.user)

    class Meta:
        db_table = 'project_participant'


class ProjectParticipantRole(models.Model):
    project_participant = models.ForeignKey(ProjectParticipant,
                                            on_delete=models.CASCADE,
                                            db_column='project_participant_id')
    project_role = models.ForeignKey(ProjectRole, on_delete=models.CASCADE, db_column='project_role_id')

    class Meta:
        db_table = 'project_participant_role'


class TaskType(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'task_type'


class TaskStatus(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'task_status'


class Task(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, db_column='project_id', related_name='tasks')
    task_creator = models.ForeignKey(ProjectParticipant,
                                     on_delete=models.PROTECT,
                                     related_name='created_tasks',
                                     db_column='task_creator_id')
    task_executor = models.ForeignKey(ProjectParticipant,
                                      on_delete=models.PROTECT,
                                      null=True,
                                      blank=True,
                                      related_name='executed_tasks',
                                      db_column='task_executor_id')
    name = models.TextField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    parent_task = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL, db_column='parent_task_id')
    description = models.TextField(null=True, blank=True)

    finish_plan_date = models.DateField()
    finish_end_date = models.DateField(null=True, blank=True)

    priority = models.IntegerField(null=True, blank=True)
    status = models.ForeignKey(TaskStatus, on_delete=models.PROTECT, db_column='status_id')
    type = models.ForeignKey(TaskType, on_delete=models.PROTECT, db_column='type_id')

    linked_tasks = models.ManyToManyField("self", blank=True, through="TaskLinkage")

    is_checked = models.BooleanField(null=True)

    def get_comments(self):
        return self.comments.order_by('created').all()

    def is_accessible_by(self, user: User):
        return self.project.is_accessible_by(user)

    def is_closed(self):
        return self.status.name == 'Закрыта'

    def set_closed(self):
        self.status = TaskStatus.objects.get(name='Закрыта')
        self.finish_end_date = datetime.datetime.now()
        self.save()

    def __str__(self):
        return f"{self.type}#{self.id} - {self.name}"

    class Meta:
        db_table = 'task'


class TaskLinkage(models.Model):
    from_task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='from_task_id')
    to_task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='to_task_id')

    class Meta:
        db_table = 'task_linkage'


class TaskAttribute(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, db_column='task_id')
    name = models.CharField(max_length=255)
    value = models.TextField()

    class Meta:
        db_table = 'task_attribute'


class Comment(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, db_column='task_id', related_name='comments')
    author = models.ForeignKey(User, on_delete=models.PROTECT, db_column='author_id')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    text = models.TextField()

    class Meta:
        db_table = 'comment'


class Webhook(models.Model):
    url = models.URLField()
    comment = models.TextField()

    def __str__(self):
        return f'{self.url} ({self.comment})'

    class Meta:
        db_table = 'webhook'


class Setting(models.Model):
    name = models.TextField()
    value = models.TextField(blank=True)
    is_set = models.BooleanField()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'setting'
