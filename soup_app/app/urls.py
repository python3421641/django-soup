from django.urls import path
from django.contrib.auth import views as auth_views


from .views import index, auth, projects, tasks, users, comments

urlpatterns = [
    path("", index.index, name="index"),

    path('user/', auth.UserView.as_view()),

    path('tasks/', tasks.index, name='task-list'),
    path('tasks/index', tasks.index),
    path('tasks/new', tasks.new),
    path('tasks/create', tasks.create),
    path('tasks/<int:id>', tasks.show, name='task-show'),
    path('tasks/<int:id>/edit', tasks.edit, name='task-edit'),
    path('tasks/<int:task_id>/set_check_success', tasks.set_check_success, name='task-set-check-success'),
    path('tasks/<int:task_id>/set_check_failure', tasks.set_check_failure, name='task-set-check-failure'),
    path('tasks/<int:task_id>/close', tasks.close, name='task-close'),
    path('tasks/project/<int:project_id>/', tasks.project_list, name='project_tasks_list'),
    path('tasks/user/<int:user_id>/', tasks.user_list, name='user_tasks_list'),

    path('comments/<int:comment_id>/edit', comments.edit, name='comment-edit'),
    path('comments/<int:task_id>/add', comments.add, name='comment-add'),

    path('projects/', projects.ProjectView.as_view(), name='project-list'),
    path('projects/<int:pk>', projects.project_detail, name='project-detail'),
    path('projects/<int:pk>/edit', projects.project_edit, name='project-edit'),
    path('projects/<int:pk>/archive', projects.project_archive, name='project-archive'),
    path('projects/<int:project_id>/participants/<int:participant_id>/edit',
         projects.participant_roles_edit,
         name='project-participant-roles-edit'),
    path('projects/<int:project_id>/participants/add', projects.participant_add, name='project-participant-add'),
    path('projects/<int:project_id>/participants/<int:participant_id>/delete',
         projects.participant_delete,
         name='project-participant-delete'),

    path("accounts/login/", auth_views.LoginView.as_view()),
    path("accounts/logout", auth.logout_view, name='logout'),
    path("accounts/change_password", auth.change_password, name='change-password'),

    path('users/list', users.UserView.as_view(), name='user-list'),
    path('users/<int:pk>', users.user_detail, name='user-detail'),
    path('users/<int:pk>/edit', users.user_edit, name='user-edit'),
]
