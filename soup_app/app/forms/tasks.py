from django import forms

from ..models import Task, TaskStatus, TaskType, Project, ProjectParticipant


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = '__all__'
        exclude = ('task_creator', 'is_checked')

    project = forms.ModelChoiceField(queryset=Project.objects.all(),
                                     label="Проект",
                                     empty_label="Выберите проект",
                                     widget=forms.Select(attrs={
                                         'class': "form-select text-center fw-bold",
                                         'style': 'max-width: auto;',
                                     }))
    type = forms.ModelChoiceField(queryset=TaskType.objects.all(),
                                  label="Тип задачи",
                                  empty_label="Выберите тип",
                                  widget=forms.Select(attrs={
                                      'class': "form-select text-center fw-bold",
                                      'style': 'max-width: auto;',
                                  }))
    status = forms.ModelChoiceField(queryset=TaskStatus.objects.all(),
                                    label="Статус задачи",
                                    empty_label="Выберите статус",
                                    widget=forms.Select(attrs={
                                        'class': "form-select text-center fw-bold",
                                        'style': 'max-width: auto;',
                                    }))
    task_executor = forms.ModelChoiceField(queryset=ProjectParticipant.objects.all(),
                                           label="Исполнитель",
                                           empty_label="Выберите исполнителя",
                                           widget=forms.Select(attrs={
                                               'class': "form-select text-center fw-bold",
                                               'style': 'max-width: auto;',
                                           }))

    name = forms.CharField(label="Название задачи", widget=forms.TextInput(attrs={
        'class': "form-control text-center fw-bold",
        'style': 'width: 99%',
        'placeholder': 'Введите название задачи'
    }))

    description = forms.CharField(label="Описание задачи", widget=forms.Textarea(attrs={
        'class': "form-control text-center fw-bold",
        'style': 'width: 99%',
        'placeholder': 'Введите описание задачи'
    }))

    finish_plan_date = forms.DateField(label='Планируемая дата завершения', widget=forms.DateInput(attrs={
        'class': "form-control text-center fw-bold",
        'style': 'max-width: auto;',
        'placeholder': 'Выберите дату',
        'type': 'text',
        'onfocus': "(this.type = 'date')"
    }))

    finish_end_date = forms.DateField(label='Фактическая дата завершения', required=False,
                                      widget=forms.DateInput(attrs={
                                          'class': "form-control text-center fw-bold",
                                          'style': 'max-width: auto;',
                                          'placeholder': 'Выберите дату',
                                          'type': 'text',
                                          'onfocus': "(this.type = 'date')"
                                      }))

    priority = forms.IntegerField(label="Приоритет", required=False, widget=forms.NumberInput(attrs={
        'class': "form-control text-center fw-bold",
        'style': 'max-width: auto;',
        'placeholder': '0',
        'min': '0',
        'max': '1000'
    }))

    parent_task = forms.ModelChoiceField(queryset=Task.objects.all(),
                                         label="Родительская задача",
                                         empty_label="Выберите задачу",
                                         required=False,
                                         widget=forms.Select(attrs={
                                             'class': "form-select text-center fw-bold",
                                             'style': 'max-width: auto;',
                                         }))

    linked_tasks = forms.MultipleChoiceField(choices=[(task.id, task) for task in Task.objects.all()],
                                             label="Связанные задачи",
                                             required=False,
                                             )
