from django import forms


class ProjectForm(forms.Form):
    name = forms.CharField(label='Полное название проекта')
    short_name = forms.CharField(max_length=255, label='Короткое название проекта')

