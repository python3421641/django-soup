from django import forms


class UserForm(forms.Form):
    last_name = forms.CharField(label='Фамилия')
    first_name = forms.CharField(label='Имя')
    second_name = forms.CharField(label='Отчество', required=False)
