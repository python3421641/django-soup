# Generated by Django 4.2.6 on 2023-12-16 14:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_alter_projectrole_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectrole',
            name='access_rights',
        ),
    ]
