from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group

from .models import *


admin.site.register(User, UserAdmin)
admin.site.register(ProjectRole)
admin.site.register(Project)
admin.site.register(ProjectParticipant)
admin.site.register(ProjectParticipantRole)
admin.site.register(TaskType)
admin.site.register(TaskStatus)
admin.site.register(Task)
admin.site.register(TaskLinkage)
admin.site.register(TaskAttribute)
admin.site.register(Comment)
admin.site.register(Webhook)


@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'is_set')


admin.site.unregister(Group)
