from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect, get_object_or_404
from django.forms.models import model_to_dict
from django.db.models import Q
from ..forms.projects import ProjectForm
from ..models import Project, ProjectParticipant, ProjectRole
from ..integrations import *


class ProjectView(LoginRequiredMixin, ListView):
    model = Project

    def get_queryset(self): 
        query = self.request.GET.get('search')
        object_list = Project.objects.all()
        if query:
            object_list = object_list.filter(
                Q(name__icontains=query) | Q(short_name__icontains=query)
            )
        return object_list


@login_required
def project_detail(request, pk):
    user = request.user
    project = get_object_or_404(Project, pk=pk)

    if not project.is_accessible_by(user):
        raise PermissionDenied()

    context = {"project": project, 'is_manageable': project.is_manageable_by(user)}
    return render(request, 'projects/project_detail.html', context)


@login_required
def project_edit(request, pk):
    user = request.user
    project = get_object_or_404(Project, pk=pk)

    if not project.is_accessible_by(user):
        raise PermissionDenied()

    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project.name = form.cleaned_data['name']
            project.short_name = form.cleaned_data['short_name']
            project.save()

            Integration.send_projects_info()

            return redirect(project_detail, pk=pk)
    else:
        form = ProjectForm(initial=model_to_dict(project))
        context = {'form': form, 'project_id': pk}
        return render(request, 'projects/project_edit.html', context)


@login_required
def project_archive(request, pk):
    user = request.user
    project = get_object_or_404(Project, pk=pk)

    if not project.is_manageable_by(user):
        raise PermissionDenied()

    if request.method == 'POST':
        active_tasks = [task for task in project.get_tasks() if not task.is_closed()]
        if active_tasks:
            context = {"active_tasks": active_tasks, "project": project}
            return render(request, 'projects/project_archive_error.html', context, status=409)
        project.archive()

        Integration.send_projects_info()

        return redirect(project_detail, pk=pk)


@login_required
def participant_roles_edit(request, project_id, participant_id):
    user = request.user
    project = get_object_or_404(Project, pk=project_id)
    participant = get_object_or_404(ProjectParticipant, pk=participant_id)

    if not project.is_manageable_by(user):
        raise PermissionDenied()

    if request.method == 'POST':
        participant_roles = request.POST.getlist('participant_roles')
        participant.roles.set(participant_roles)

        return redirect(project_detail, pk=project_id)
    else:
        all_possible_roles = ProjectRole.objects.all()
        context = {"project": project, "edited_participant": participant, "all_possible_roles": all_possible_roles}

        return render(request, 'projects/project-participant-roles-edit.html', context)


@login_required
def participant_add(request, project_id):
    user = request.user
    project = get_object_or_404(Project, pk=project_id)

    if not project.is_manageable_by(user):
        raise PermissionDenied()

    context = {"project": project, "roles": ProjectRole.objects.all()}

    if request.method == 'POST':
        participant_user_id = request.POST.get('participant_user_id')
        participant_roles = request.POST.getlist('participant_roles')

        participant_object = ProjectParticipant(
            user_id=participant_user_id,
            project_id=project.id
        )
        participant_object.save()
        participant_object.roles.add(*participant_roles)

        return redirect(project_detail, pk=project_id)
    else:
        return render(request, 'projects/project-participant-add.html', context)


@login_required
def participant_delete(request, project_id, participant_id):
    if request.method == 'POST':
        user = request.user
        project = get_object_or_404(Project, pk=project_id)

        if not project.is_manageable_by(user):
            raise PermissionDenied()

        participant = get_object_or_404(ProjectParticipant, pk=participant_id)
        participant.delete()
        return redirect(project_detail, pk=project_id)

    else:
        return HttpResponse(405)
