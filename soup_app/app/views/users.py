from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect, get_object_or_404
from django.forms.models import model_to_dict
from django.db.models import Q
from ..forms.users import UserForm
from ..models import Project, User


class UserView(LoginRequiredMixin, ListView):
    model = User

    template_name = 'users/list.html'

    def get_queryset(self):
        query = self.request.GET.get('search')
        object_list = User.objects.all()
        if query:
            object_list = object_list.filter(
                Q(username__icontains=query) |
                Q(first_name__icontains=query) |
                Q(second_name__icontains=query) |
                Q(last_name__icontains=query)
            )
        return object_list.order_by('id')


@login_required
def user_detail(request, pk):
    # user = request.user
    requested_user = get_object_or_404(User, pk=pk)
    context = {"user": requested_user}

    return render(request, 'users/user_detail.html', context)


@login_required
def user_edit(request, pk):
    user = request.user

    if user.id != pk:
        raise PermissionDenied()

    if request.method == 'POST':
        form = UserForm(request.POST)

        if form.is_valid():
            user.first_name = form.cleaned_data['first_name']
            user.second_name = form.cleaned_data['second_name']
            user.last_name = form.cleaned_data['last_name']

            user.save()

            return redirect(user_detail, pk=pk)
    else:
        form = UserForm(initial=model_to_dict(user))
        context = {'form': form}
        return render(request, 'users/user-edit.html', context=context)
