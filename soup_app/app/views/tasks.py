from datetime import datetime

from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django import forms
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from ..models import Task, TaskStatus, TaskType, Project, ProjectParticipant
from ..helpers import get_object_or_none
from django.forms.models import model_to_dict

from ..models import Task, TaskStatus, TaskType, Project, ProjectParticipant
from ..forms.tasks import TaskForm


@login_required
def index(request):
    tasks = Task.objects.all()
    return render(request, "tasks/index.html", {"tasks": tasks})


@login_required
def new(request):
    form = TaskForm()
    return render(request, "tasks/create.html", {"form": form})


@login_required
def create(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            if form.cleaned_data["task_executor"].project.id != form.cleaned_data["project"].id:
                form.add_error("task_executor", "Исполнитель должен быть участником проекта!")
                return render(request, "tasks/create.html", {"form": form})

            current_datetime = datetime.now()
            task = Task()

            user = request.user
            task.task_creator = ProjectParticipant.objects.get(user_id=user.id)

            task.name = form.cleaned_data["name"]
            task.description = form.cleaned_data["description"]
            task.task_executor = form.cleaned_data["task_executor"]
            task.finish_plan_date = form.cleaned_data["finish_plan_date"]
            task.finish_end_date = form.cleaned_data["finish_end_date"]
            task.priority = form.cleaned_data["priority"]
            task.project = form.cleaned_data["project"]
            task.type = form.cleaned_data["type"]
            task.status = form.cleaned_data["status"]
            task.parent_task = form.cleaned_data["parent_task"]

            task.created, task.updated = current_datetime, current_datetime

            task.save()

            for linked_task in form.cleaned_data["linked_tasks"]:
                task.linked_tasks.add(int(linked_task))
            return HttpResponseRedirect(f"/tasks/{task.id}")
    else:
        form = TaskForm()
        return render(request, "create.html", {"form": form})


@login_required
def edit(request, id):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = get_object_or_404(Task, pk=id)
            if form.cleaned_data["task_executor"].project.id != form.cleaned_data["project"].id:
                form.add_error("task_executor", "Исполнитель должен быть участником проекта!")
                return render(request, "tasks/edit.html", {"form": form, "task": task})
            current_datetime = datetime.now()

            task.name = form.cleaned_data["name"]
            task.description = form.cleaned_data["description"]
            task.task_executor = form.cleaned_data["task_executor"]
            task.finish_plan_date = form.cleaned_data["finish_plan_date"]
            task.finish_end_date = form.cleaned_data["finish_end_date"]
            task.priority = form.cleaned_data["priority"]
            task.project = form.cleaned_data["project"]
            task.type = form.cleaned_data["type"]
            task.status = form.cleaned_data["status"]
            task.parent_task = form.cleaned_data["parent_task"]
            for linked_task in form.cleaned_data["linked_tasks"]:
                task.linked_tasks.add(int(linked_task))

            if task.status.name == 'Закрыта' and task.finish_end_date is None:
                task.finish_end_date = current_datetime

            task.updated = current_datetime

            task.save()
            return HttpResponseRedirect(f"/tasks/{task.id}")
    else:
        task = get_object_or_404(Task, pk=id)
        form = TaskForm(initial=model_to_dict(task))
        return render(request, "tasks/edit.html", {"form": form, "task": task})


@login_required
def show(request, id):
    task = get_object_or_404(Task, pk=id)
    return render(request, "tasks/show.html", {"task": task})


@login_required
def project_list(request, project_id):
    user = request.user
    project = get_object_or_404(Project, pk=project_id)

    if not project.is_accessible_by(user):
        raise PermissionDenied()
    
    tasks = Task.objects.all()
    proj = Project.objects.all()
    task_types = TaskType.objects.all()
    task_statuses = TaskStatus.objects.all()
    tasks = tasks.filter(project = project_id)
    proj = proj.get(id = project_id)
    query = request.GET.get('search')
    if query:
            tasks = tasks.filter(
                Q(name__icontains=query)
            )
    return render(request, "tasks/project_tasks_list.html", {"tasks": tasks, "project": project_id, 
                                                             "task_types": task_types, "task_statuses": task_statuses,
                                                             "proj": proj})


@login_required
def user_list(request, user_id):
    if request.user.id != user_id:
        raise PermissionDenied()

    tasks = Task.objects.all()
    task_types = TaskType.objects.all()
    task_statuses = TaskStatus.objects.all()
    query = request.GET.get('search')
    if query:
            tasks = tasks.filter(
                Q(name__icontains=query)
            )
    return render(request, "tasks/user_tasks_list.html", {"tasks": tasks, "task_types": task_types, 
                                                             "task_statuses": task_statuses})


@login_required
def set_check_success(request, task_id):
    user = request.user

    task = get_object_or_404(Task, pk=task_id)
    if task.task_creator.user != user:
        raise PermissionDenied()

    if request.method == 'POST':
        task.is_checked = True
        task.save()

        return redirect(show, id=task_id)

    else:
        return HttpResponse(405)


@login_required
def set_check_failure(request, task_id):
    user = request.user

    task = get_object_or_404(Task, pk=task_id)
    if task.task_creator.user != user:
        raise PermissionDenied()

    if request.method == 'POST':
        task.is_checked = False
        task.save()

        return redirect(show, id=task_id)

    else:
        return HttpResponse(405)


def close(request, task_id):
    user = request.user

    task = get_object_or_404(Task, pk=task_id)
    if task.task_creator.user != user:
        raise PermissionDenied()

    if request.method == 'POST':
        task.set_closed()

        return redirect(show, id=task_id)

    else:
        return HttpResponse(405)
