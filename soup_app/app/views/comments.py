from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect, get_object_or_404
from django.forms.models import model_to_dict
from django.db.models import Q
from ..forms.projects import ProjectForm
from ..models import Project, ProjectParticipant, ProjectRole, Task, Comment


@login_required
def comment_list(request, task_id):
    user = request.user
    task = get_object_or_404(Task, pk=task_id)

    if not task.is_accessible_by(user):
        raise PermissionDenied()

    comments = Comment.objects.filter(task_id=task_id).order_by('created').all()

    return render(request, 'comments/list.html', {'comments': comments})


@login_required
def edit(request, comment_id):
    user = request.user
    edited_comment = get_object_or_404(Comment, pk=comment_id)

    if not edited_comment.task.is_accessible_by(user):
        raise PermissionDenied()

    if request.method == 'POST':
        new_text = request.POST.get('comment-text')
        edited_comment.text = new_text
        edited_comment.save()

        return redirect('task-show', edited_comment.task_id)

    else:
        return render(request, 'comments/edit.html', {'edited_comment': edited_comment, 'task': edited_comment.task})


@login_required
def add(request, task_id):
    user = request.user

    task = get_object_or_404(Task, pk=task_id)
    if not task.is_accessible_by(user):
        raise PermissionDenied()

    if request.method == 'POST':
        text = request.POST.get('comment-text')
        comment = Comment(text=text, author=user, task=task)
        comment.save()

        return redirect('task-show', id=task_id)
    else:
        return HttpResponse(405)
