from django.shortcuts import render
from django.http import HttpResponse
from django import forms
from django.contrib.auth.decorators import login_required


@login_required
def index(request):
    user = request.user
    return render(request, "index.html", {"user": user})
