export WORK_DIR=$(pwd)

if [ ! -d "./volumes" ]; then
  mkdir volumes
  echo "volumes directory not found. Creating it."
fi

if [ ! -d "./volumes/db" ]; then
  mkdir volumes/db
  echo "volumes/db directory not found. Creating it."
fi

echo "Unlocking secrets..."
git-crypt unlock ../git-crypt-key
echo "Setup done. Now run 'docker compose -f *.yml up -d' to launch the application."
